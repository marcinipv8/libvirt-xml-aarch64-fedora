# libvirt-xml-aarch64-fedora

This is a sample libvirt .xml file for booting arm64 images on an amd64 machine, using
libvirt and qemu-system-aarch64.

You will probably want to customize it.

## Requirements

* qemu-system-aarch64-core
* edk2-aarch64
